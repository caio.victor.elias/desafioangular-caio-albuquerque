# Desafios Encontrados
Dentre as dificuldades encontradas a maior foi na criatividade em como seriam exibidas as informações. A busca por entregar algo bem estruturado com guardas de rotas, verificações de login visto que não tinha um serviço específico, e demais detalhes que passam despercebidos quando se trabalha muito em um projeto já iniciado, mas que merecem total atenção no início de um novo projeto.
Acho que acabei focando muito em estruturar o ambiente, mais do que exibir as informações. Fora isso só alguns problemas de cunho pessoal.
