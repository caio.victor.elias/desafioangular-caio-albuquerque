import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from './model/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type':'application/json'})
  };

  API: string = "https://combustivelapp.herokuapp.com/api";
  usuarioLogado: Usuario = new Usuario();
  constructor(
    private readonly http: HttpClient,
  ) {

  }

}
