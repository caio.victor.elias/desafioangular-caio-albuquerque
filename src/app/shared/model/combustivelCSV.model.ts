export class CombustivelCSV{
    bandeira:	string;
    codInstalacao: number;
    dataColeta:	string;//($date-time)
    id: number;
    municipio:	string;
    produto:	string;
    revendedora:	string;
    siglaEstado:	string;
    siglaRegiao:	string;
    unidadeMedida:	string;
    valorCompra:	number;
    valorVenda:	number;
}