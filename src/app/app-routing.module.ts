import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/authguard';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { PaginaNaoEncontradaComponent } from './pages/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { UsuarioComponent } from './pages/usuario/usuario.component';

export const routes: Routes = [

  { path : 'login', component: LoginComponent },

  // { path : 'usuario' , component: UsuarioComponent,
  //   // canActivate: [AuthGuard],
  //   // canActivateChild: [AuthGuard],
  //   children : [
  //     { path : ':id' , component: UsuarioComponent},
  //   ]
  // },

  // { path : 'combustivel' ,
  //   // canActivate: [AuthGuard],
  //   canLoad: [AuthGuard],
  //   loadChildren: () => import('./pages/combustivel/combustivel.module').then(m => m.CombustivelModule)
  // },

  // { path : 'historico' ,
  //   // canActivate: [AuthGuard],
  //   canLoad: [AuthGuard],
  //   loadChildren: () => import('./pages/historico/historico.module').then(m => m.HistoricoModule)
  // },

  { path : '' ,
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    component: HomeComponent
  },

  { path : '**', component: PaginaNaoEncontradaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
