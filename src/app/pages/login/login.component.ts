import { UtilService } from './../../shared/util.service';
import { UsuarioDTO } from './../../shared/model/usuariodto.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from './../../services/usuario/usuario.service';
import { LoginService } from './../../services/login/login.service';
import { Usuario } from './../../shared/model/usuario.model';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  authForm: FormGroup;
  signinForm: FormGroup;

  hasError: boolean = false;
  carregando: boolean = false;
  mensagemRetorno: string = '';
  isLogin: boolean = true;//se não for login é sign in
  exibirSucesso: boolean = false;

  constructor(
    private loginservice : LoginService,
    private usuarioService: UsuarioService,
    private formBuilder: FormBuilder,
    private utilService : UtilService,
    private router: Router
  ) {
    this.authForm = this.formBuilder.group({
      email: ['', Validators.required],
      login: [null, Validators.required],
      // password: [null, Validators.required]
    })
    this.signinForm = this.formBuilder.group({
      nome: ['', Validators.required],
      email: ['', Validators.required],
      login: [null, Validators.required],
      relogin: [null, Validators.required],
      // password: [null, Validators.required],
      // repassword: [null, Validators.required]
    })

  }

  ngOnInit(): void {
  }

  trocar(){
    if(!this.carregando){
      this.isLogin = !this.isLogin;
    }
  }

  fazerlogin(){
    this.hasError = false;
    this.carregando = true;
    this.mensagemRetorno = "";
    if(this.authForm.valid){

      //O melhor caso seria um serviço que buscasse apenas as informações do usuário passado como parâmetro
      //Ou um serviço que buscasse pelo email
      //Na ausencia destes serviços, optei por buscar dentre a lista de usuários existentes
      this.carregando = true;
      this.usuarioService.getTodosUsuarios().subscribe((data:Usuario[])=>{
        if(data){
          for (let i = 0; i < data.length; i++) {
            if(data[i].login == this.authForm.value.login &&
              data[i].email == this.authForm.value.email){
                this.loginservice.usuarioAutenticado = true;
                this.utilService.usuarioLogado = data[i];
                this.router.navigateByUrl("/");
                break;
            }
          }
          if(!this.loginservice.usuarioAutenticado){
            this.hasError = true;
            this.mensagemRetorno = "Usuário não encontrado. Verifique o usuário e a senha ou crie uma nova conta."
          }
        }else{
          this.hasError = true;
          this.loginservice.usuarioAutenticado = false;
          this.mensagemRetorno = "Algo deu errado. Verifique o usuário e a senha.";
        }
      },error =>{
        this.carregando = false;
        this.hasError = true;
        this.loginservice.usuarioAutenticado = false;
        this.mensagemRetorno = "Algo deu errado. Verifique o email e login.";
      }, ()=>{
        this.carregando = false;
      })
    }else{
      this.hasError = true;
      this.carregando = false;
      this.mensagemRetorno = "Insira um email e login.";
    }
  }

  criarConta(){
    this.hasError = false;
    this.mensagemRetorno = "";
    if(this.signinForm.valid && this.signinForm.value.login == this.signinForm.value.relogin){
      //chamar serviço de criar usuario
      this.carregando = true;
      this.usuarioService.getTodosUsuarios().subscribe((data:Usuario[])=>{
        let podeCriar = true;
        if(data){
          for (let i = 0; i < data.length; i++) {
            if(data[i].email == this.authForm.value.email){
              podeCriar = false;
              break;
            }
          }
        }else{
          podeCriar = true;
        }
        if(podeCriar){
          let newUser : UsuarioDTO = {
            nome: this.signinForm.value.nome,
            email: this.signinForm.value.email,
            login: this.signinForm.value.login,
            senha: this.signinForm.value.login//medida provisória por nao poder ser vazio e nao poder utilizar no momento de verificacao no login
          }
          this.usuarioService.createUsuario(newUser).subscribe(data=>{
            this.signinForm.reset();
            this.isLogin = true;
            this.exibirSucesso = true;
            setTimeout(() => {
              this.exibirSucesso = false;
            }, 3000);
          },error => {
            this.carregando = false;
            this.hasError = true;
            this.mensagemRetorno = "Algo deu errado. Verifique o email e login.";
          },()=>{
            this.carregando = false;
          })
        }else{
          this.carregando = false;
          this.hasError = true;
          this.mensagemRetorno = "Usuário já existe. Por favor, entre com este usuário ou crie um novo";
        }
      });
    }else{
      this.carregando = false;
      this.hasError = true;
      this.mensagemRetorno = "Verifique se login está igual a confirmação";
    }
  }
}
