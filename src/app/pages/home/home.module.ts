import { CombustivelComponent } from './../combustivel/combustivel.component';
import { CombustivelModule } from './../combustivel/combustivel.module';
import { HomeComponent } from './home.component';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    HomeComponent,
    CombustivelComponent
  ],
  imports: [
    CommonModule,
    // CombustivelModule
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    // { provide: LOCALE_ID, useValue: "pt-BR" }
  ],
})
export class HomeModule { }
