import { CombustivelService } from './../../services/combustivel/combustivel.service';
import { UtilService } from './../../shared/util.service';
import { Usuario } from './../../shared/model/usuario.model';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login/login.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Media } from 'src/app/shared/model/media.model';
import { CombustivelCSV } from 'src/app/shared/model/combustivelCSV.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  usuarioLogado: Usuario = new Usuario();
  showdropdown: boolean = false;

  listaMunicipios: Media[] = [];
  municipioSelecionado: Media = new Media();
  listDistribuidoras: string[] = [];
  listAll: CombustivelCSV[] = [];
  distribuidoraSelecionada: string;
  mostrarTabela: boolean = false;
  mediaCompraByDist: number = 0;
  mediaVendaByDist: number = 0;
  mediaPreco: number = 0;

  dadosBydist: CombustivelCSV[] = [];
  listheadersByDist: any[] = [
    "Bandeira",//: "BRANCA"
    "Instalacao",//: 9551
    "Data Coleta",//: "2018-01-03T00:00:00.000+0000"
    // "id",//: 242
    "Município",//: "GOIANIA"
    "Produto",//: "DIESEL"
    "Revendedora",//: "3AUTO POSTO PEDRO LUDOVICO LTDA"
    "Estado",//: "GO"
    "Região",//: "CO"
    "Unidade",//Medida: "R$ / litro"
    "Compra",//: 0
    "Venda",//: 3.299
  ];

  constructor(
    private loginservice : LoginService,
    private utilService : UtilService,
    private usuarioService: UsuarioService,
    private combustivelService: CombustivelService,
    private router: Router
  ) {

  }

  ngOnInit(): void {
    this.usuarioLogado = this.utilService.usuarioLogado;
    this.getMunicipios();
    this.getMediaPorDistribuidoras();
  }

  sair(){
    this.loginservice.usuarioAutenticado = false;
    this.router.navigateByUrl("/login");
  }

  showMenu(){
    this.showdropdown = !this.showdropdown;
  }

  delete(){
    //deveria ser feito uma confirmacao antes de deletar
    this.usuarioService.deleteUsuario(this.usuarioLogado.id).subscribe(data=>{
      this.loginservice.usuarioAutenticado = false;
      this.router.navigateByUrl("/login");
    },error =>{
      //tratar erro
      this.loginservice.usuarioAutenticado = false;
      this.router.navigateByUrl("/login");
    })
  }

  getMunicipios(){
    this.combustivelService.getValorMedioCompraVendaPorMunicipio().subscribe((data:Media[])=>{
      this.listaMunicipios = data;
    })
  }

  getMediaPorDistribuidoras(){
    this.listDistribuidoras = [];
    this.listAll = [];
    this.combustivelService.getCombustivelPorDistribuidora().subscribe((data:CombustivelCSV[])=>{
      this.listAll = data;
      let newlistDist = [];
      data.forEach(element => {
        newlistDist.push(
          element.revendedora
        );
      });
      this.listDistribuidoras = newlistDist.filter((este, i) => newlistDist.indexOf(este) === i);
    })
  }

  getDistribuidora(){
    let listByDist = [];
    this.dadosBydist = [];
    let totalMediaCompra = 0;
    let totalMediaVenda = 0;
    this.listAll.forEach(element => {
      if(element.revendedora == this.distribuidoraSelecionada){
        listByDist.push(element);
        totalMediaCompra += element.valorCompra;
        totalMediaVenda += element.valorVenda;
      }
    });
    this.dadosBydist = listByDist;

    this.mediaCompraByDist = this.dadosBydist.length>0 ? totalMediaCompra/this.dadosBydist.length : 0;
    this.mediaVendaByDist = this.dadosBydist.length>0 ? totalMediaVenda/this.dadosBydist.length : 0;
  }

  selecionarMunicipio(){
    for (let i = 0; i < this.listaMunicipios.length; i++) {
      if(this.listaMunicipios[i].atributo == this.municipioSelecionado.atributo){
        this.municipioSelecionado.atributo = this.listaMunicipios[i].atributo;
        this.municipioSelecionado.mediaValorCompra = this.listaMunicipios[i].mediaValorCompra;
        this.municipioSelecionado.mediaValorVenda = this.listaMunicipios[i].mediaValorVenda;
        break;
      }
    }

    //Serviço desnecessário até entao visto que o valor é igual a média do valor de venda
    // this.combustivelService.getMediaPrecoPorMunicipio(this.municipioSelecionado.atributo).subscribe((data:number)=>{
    //   if(data) 
    //     this.mediaPreco = data;
    //   else
    //     this.mediaPreco = 0;
    // },error =>{
    //   this.mediaPreco = 0;
    // })
  }

}
