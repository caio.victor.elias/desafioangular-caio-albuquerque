import { HistoricoComponent } from './historico.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    HistoricoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HistoricoModule { }
