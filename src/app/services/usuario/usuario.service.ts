import { Usuario } from './../../shared/model/usuario.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilService } from './../../shared/util.service';
import { UsuarioDTO } from 'src/app/shared/model/usuariodto.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private readonly http: HttpClient,
    private utilService: UtilService
  ) { }

  getTodosUsuarios(){
    return this.http.get(this.utilService.API+"/usuarios");
  }

  createUsuario(usuario: UsuarioDTO){
    return this.http.post(this.utilService.API+"/usuarios", usuario);
  }

  updateUsuario(usuario: Usuario){
    return this.http.put(this.utilService.API+"/usuarios", usuario);
  }

  getUsuarioPorId(id){
    // let params = new HttpParams();
    // params = params.append("id", id);
    return this.http.get(this.utilService.API+"/usuarios/"+id);
  }

  deleteUsuario(id){
    // let params = new HttpParams();
    // params = params.append("id", id);
    return this.http.delete(this.utilService.API+"/usuarios/"+id);
  }
}
