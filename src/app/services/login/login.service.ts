import { UsuarioService } from './../usuario/usuario.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Usuario } from './../../shared/model/usuario.model';
import { UtilService } from './../../shared/util.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  usuarioAutenticado: boolean = false;
  carregando = false;

  constructor(
    private readonly http: HttpClient,
    private utilService: UtilService,
    private usuarioService: UsuarioService
  ) { }

  //endpoint não implementado
  // fazerLogin(usuario : Usuario){
  // }

  usuarioEstaAutenticado(){
    return this.usuarioAutenticado;
  }
}
