import { CombustivelCSV } from './../../shared/model/combustivelCSV.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilService } from './../../shared/util.service';

@Injectable({
  providedIn: 'root'
})
export class CombustivelService {

  constructor(
    private readonly http: HttpClient,
    private utilService: UtilService
  ) { }

  getCombustivelPorData(){
    return this.http.get(this.utilService.API+"/combustivel/dados-agrupados-por-data-coleta");
  }

  getCombustivelPorDistribuidora(){
    return this.http.get(this.utilService.API+"/combustivel/dados-agrupados-por-distribuidora");
  }

  getCombustivelPorSigla(sigla:string){
    return this.http.get(this.utilService.API+"/combustivel/dados-por-sigla/"+sigla);
  }

  getMediaPrecoPorMunicipio(municipio:string){
    return this.http.get(this.utilService.API+"/combustivel/media-de-preco/"+municipio);
  }

  getValorMedioCompraVendaPorBandeira(){
    return this.http.get(this.utilService.API+"/combustivel/valor-media-compra-venda-bandeira");
  }

  getValorMedioCompraVendaPorMunicipio(){
    return this.http.get(this.utilService.API+"/combustivel/valor-media-compra-venda-municipio");
  }
}
