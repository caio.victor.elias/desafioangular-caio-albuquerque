import { Historico } from './../../shared/model/historico.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilService } from './../../shared/util.service';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  constructor(
    private readonly http: HttpClient,
    private utilService: UtilService
  ) { }

  getTodosHistoricos(){
    return this.http.get(this.utilService.API+"/historico");
  }

  createHistorico(historico: Historico){
    return this.http.post(this.utilService.API+"/historico", historico);
  }

  updateHistorico(historico: Historico){
    return this.http.put(this.utilService.API+"/historico", historico);
  }

  getHistoricoPorId(id){
    // let params = new HttpParams();
    // params = params.append("id", id);
    return this.http.get(this.utilService.API+"/historico/"+id);
  }

  deleteHistorico(id){
    // let params = new HttpParams();
    // params = params.append("id", id);
    return this.http.delete(this.utilService.API+"/historico/"+id);
  }
}
